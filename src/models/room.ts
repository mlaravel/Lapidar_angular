import { TypeRoom } from './type-room';
export interface Room {
    windows: string;
    door: string;
    cd_apartment: string;
    room: TypeRoom;
}
