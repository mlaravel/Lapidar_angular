import { GeneralizacaoRoutingModule } from './home-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneralizacaoComponent } from './generalizacao.component';

@NgModule({
  imports: [
    CommonModule,
    GeneralizacaoRoutingModule
  ],
  declarations: [GeneralizacaoComponent]
})
export class GeneralizacaoModule { }
