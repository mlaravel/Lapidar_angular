import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-screen-size',
  templateUrl: './screen-size.component.html',
  styleUrls: ['./screen-size.component.css']
})
export class ScreenSizeComponent implements OnInit {

  public newInnerHeight: number;
  public newInnerWidth: number;
  constructor() {
    const screenHeight = window.screen.height;
    const screenWidth = window.screen.width;
    console.log(screenHeight);
    console.log(screenWidth);
   }

  ngOnInit() {
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.newInnerHeight = event.target.innerHeight;
    this.newInnerWidth = event.target.innerWidth;
  }

}
