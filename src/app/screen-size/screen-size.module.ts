import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScreenSizeRoutingModule } from './screen-size-routing.module';
import { ScreenSizeComponent } from './screen-size/screen-size.component';

@NgModule({
  imports: [
  CommonModule,
    ScreenSizeRoutingModule
  ],
  declarations: [ScreenSizeComponent]
})
export class ScreenSizeModule { }
