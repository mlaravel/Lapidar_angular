import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScreenSizeComponent } from './screen-size/screen-size.component';

const routes: Routes = [
  { path: '', component: ScreenSizeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
exports: [RouterModule]
})
export class ScreenSizeRoutingModule { }
