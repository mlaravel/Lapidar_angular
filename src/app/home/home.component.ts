import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public array: any[] = [
    {id: 1, nome: 'Crane'},
    {id: 2, nome: 'Manuel'},
    {id: 3, nome: 'José'},
    {id: 4, nome: 'Maria'},
    {id: 5, nome: 'Sonia'},
    {id: 6, nome: 'Joana'},
  ];

  public ids: any[] = [2, 3];

  constructor() { }

  ngOnInit() {
    this.removePessoas();
  }

  public removePessoas(): void {
    this.ids.forEach(element => {
      const index = this.array.findIndex(x => x.id === element);
      this.array.splice(index, 1);
    });
  }

}
