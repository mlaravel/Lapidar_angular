import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GroupRoutingModule } from './group-routing.module';
import { GroupComponent } from './group/group.component';

import { FormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    GroupRoutingModule,
    FormsModule
  ],
  declarations: [GroupComponent]
})
export class GroupModule { }
