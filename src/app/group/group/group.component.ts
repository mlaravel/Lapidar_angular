import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit {

  constructor() { }
  public males = 3;
  public females = 15;
  public group = null;

  ngOnInit() {
    const total = this.males + this.females;
    console.log(total);
    if (total === 0) {
      this.group = 0;
      return;
    }
    if ( total % 5 === 0) {
      if (this.males % 5 === 0) {
        this.males = (this.males / 5);
      }
      if (this.females % 5 === 0) {
        this.females = (this.females / 5);
      }
      this.group = 5;
    } else if ( total % 4 === 0) {
      if (this.males % 4 === 0) {
        this.males = (this.males / 4);
      }
      if (this.females % 4 === 0) {
        this.females = (this.females / 4);
      }
      this.group = 4;
    }  else if ( total % 3 === 0) {
      if (this.males % 3 === 0) {
        this.males = (this.males / 3);
      }
      if (this.females % 3 === 0) {
        this.females = (this.females / 3);
      }
      this.group = 3;
    }  else if ( total % 2 === 0) {
      if (this.males % 2 === 0) {
        this.males = (this.males / 2);
      }
      if (this.females % 2 === 0) {
        this.females = (this.females / 2);
      }
      this.group = 2;
    }  else if ( total % 1 === 0) {
      this.group = 1;
    }  else {
      this.group = 1;
    }
  }

}
