import { AppComponent } from './../app.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'app', component: AppComponent },
  { path: 'home', loadChildren: '../home/home.module#HomeModule' },
  { path: 'generalizacao', loadChildren: '../generalizacao/generalizacao.module#GeneralizacaoModule' },
  { path: 'group', loadChildren: '../group/group.module#GroupModule'},
  { path: 'screen-size', loadChildren: '../screen-size/screen-size.module#ScreenSizeModule'},
  { path: '', redirectTo: '/app', pathMatch: 'full' },
];

@NgModule({
  imports: [
    CommonModule,
    [ RouterModule.forRoot(routes) ]
  ],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }
